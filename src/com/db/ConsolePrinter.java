package com.db;

public class ConsolePrinter {
    public static void printNewLine(){
        System.out.println("\n");
    }

    public static void authenticationLoginOrSignUpHeader(){
        System.out.println("-----------------------------------------");
        System.out.println("---------- PLEASE LOGIN/SIGNUP ----------");
        System.out.println("-----------------------------------------");
    }

    public static void authenticationLoginOrSignUpChoiceSelection(){
        System.out.println("-> Press 1 to LOGIN");
        System.out.println("-> Press 2 to SIGNUP");
        System.out.print("Enter your choice: ");
    }

    public static void authenticationSignUpHeader(){
        System.out.print("\n\n");
        System.out.println("-----------------------------------------");
        System.out.println("---------------- SIGNUP -----------------");
        System.out.println("-----------------------------------------");
    }

    public static void authenticationLoginHeader(){
        System.out.print("\n\n");
        System.out.println("-----------------------------------------");
        System.out.println("---------------- Login ------------------");
        System.out.println("-----------------------------------------");
    }

    public static void authenticationLoginSuccess(){
        System.out.print("\n");
        System.out.println("Successfully Logged In...");
        System.out.println("Now you can perform operation like CREATE, SELECT, INSERT, UPDATE, DELETE...");
        System.out.print("\n");
    }

    public static void authenticationSignUpSuccess(){
        System.out.print("\n");
        System.out.println("Successfully Signed Up...");
        System.out.print("\n");
    }
    
    public static void tableCreateSuccess(){
        System.out.print("\n");
        System.out.println("Successfully Table Created...");
        System.out.print("\n");
    }

    public static void tableInsertSuccess(){
        System.out.print("\n");
        System.out.println("Successfully Inserted data...");
        System.out.print("\n");
    }

    public static void tableUpdateSuccess(){
        System.out.print("\n");
        System.out.println("Successfully Updated data...");
        System.out.print("\n");
    }


    public static void authenticationSignUpLoginUsername(){
        System.out.print("1. Enter username: ");
    }

    public static void authenticationSignUpLoginPassword(){
        System.out.print("2. Enter password: ");
    }

    public static void authenticationSignUpLogin2FA(){
        System.out.println("Note: Next question is for 2FA");
        System.out.print("3. What city were you born in? ");
    }

    public static void databaseOperationSelectHeader(){
        System.out.println("-----------------------------------------");
        System.out.println("---------- SELECT DB OPERATION ----------");
        System.out.println("-----------------------------------------");
    }

    public static void databaseOperationSelectOptions(){
        System.out.println("-> Press 1 to CREATE DATABASE");
        System.out.println("-> Press 2 to CREATE TABLE");
        System.out.println("-> Press 3 to SELECT");
        System.out.println("-> Press 4 to INSERT");
        System.out.println("-> Press 5 to UPDATE");
        System.out.println("-> Press 6 to DELETE");
        System.out.println("-> Press 7 to QUIT");
        System.out.print("Enter your choice: ");
    }


    public static void databaseOperationSelectedOption(String text){
        System.out.println("---------- " + text +" ----------");
        System.out.println("Please enter query below: ");
    }


    public static void databaseCreateSuccess(){
        System.out.println("Database created successfully...");
    }
}
