package com.db;

import java.io.*;
import java.util.ArrayList;

public class FileData {

    public static ArrayList<ArrayList<Object>> fetchDataFromFile(String filePath){
        ArrayList<ArrayList<Object>> data = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            String line;

            while ((line = br.readLine()) != null) {
                String[] values = line.split(" -\\*- ");
                ArrayList<Object> row = new ArrayList<>();
                for (String value : values) {
                    row.add(value);
                }
                data.add(row);
            }
        } catch (Exception e) {
            System.out.println("Error in reading file: " + e.getMessage());
        }

        return data;
    }

    public static boolean insertDataInFile(String filePath, String data, String tableName){
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(filePath));
            writer.write(data);
            writer.close();
            return true;
        } catch (IOException e) {
            System.out.println("An error occurred while writing to table: " + tableName);
            return false;
        }
    }

}
