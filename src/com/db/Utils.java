package com.db;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.math.BigInteger;

public class Utils {
    public static String buildInsertDataStringFromObjectArrayList(ArrayList<ArrayList<Object>> data){
        StringBuilder stringBuilder = new StringBuilder();

        // Join the inner lists into strings with the delimiter
        for (ArrayList<Object> row : data) {

            // Convert the list to an array of CharSequence
            CharSequence[] array = new CharSequence[row.size()];
            for (int i = 0; i < row.size(); i++) {
                String str = "";
                if(row.get(i) != null){
                    str = row.get(i).toString();
                }
                array[i] = str;
            }

            // Merge row1 into a single string with the delimiter -*-
            String mergedRow = String.join(" -*- ", array);
            stringBuilder.append(mergedRow);
            stringBuilder.append("\n");
        }

        return stringBuilder.toString();
    }

    public static String buildInsertDataStringFromStringArrayList(ArrayList<ArrayList<String>> data) {
        StringBuilder stringBuilder = new StringBuilder();

        // Join the inner lists into strings with the delimiter
        for (ArrayList<String> row : data) {

            // Merge row1 into a single string with the delimiter -*-
            String mergedRow = String.join(" -*- ", row);
            stringBuilder.append(mergedRow);
            stringBuilder.append("\n");
        }

        return stringBuilder.toString();
    }

        // this method currently only checks for Integer and String
    public static boolean isObjectEmpty(Object value){
        if(value instanceof Integer || value instanceof String){
            return value.toString().length() == 0;
        }

        return false;
    }

    public static String getMd5Hash(String input){
        String hash = null;
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");

            byte[] messageDigestByteArr = messageDigest.digest(input.getBytes());

            BigInteger bigIntNo = new BigInteger(1, messageDigestByteArr);

            hash = bigIntNo.toString(16);
            while (hash.length() < 32) {
                hash = "0" + hash;
            }

            return hash;
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    public static String getMultiLineInputFromConsole(){
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        StringBuilder input = new StringBuilder();
        String line = null;
        while (true) {
            try {
                if (!((line = br.readLine()) != null && !line.isEmpty())) break;
            } catch (IOException e) {
                e.printStackTrace();
            }
            input.append(line).append(System.lineSeparator());
        }

        return input.toString();
    }
}
