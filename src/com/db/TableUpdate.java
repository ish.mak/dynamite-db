package com.db;

import java.util.ArrayList;
import java.util.Map;

public class TableUpdate {
    private String schemaName;
    private String tableName;
    private String updateTableString;
    private String columnNameToSet;
    private String valueToSet;
    private int setColumnIndex;
    private String valueToMatch;
    private String columnNameToMatch;
    private int matchColumnIndex;


    public TableUpdate(String updateTableString){
        this.updateTableString = updateTableString;
    }

    public boolean parseQuery(){
        String newUpdateTableString = updateTableString.replaceAll("\\n","");

        // Remove the trailing semicolon
        newUpdateTableString = newUpdateTableString.replaceAll(";$", "");

        // Split the input into different parts
        String[] parts = newUpdateTableString.split(" SET ");

        // Extract the first part
        String update = parts[0];

        // Extract the second and third parts
        String[] subParts = parts[1].split(" WHERE ");
        String set = subParts[0];
        String where = subParts[1];

        // Split the input into different parts by space
        String[] updateAndSchemaTableNameParts = update.split(" ");

        // Extract the table name and schema name
        String[] tableAndSchema = updateAndSchemaTableNameParts[1].split("\\.");
        schemaName = tableAndSchema[0];
        tableName = tableAndSchema[1];


        // Split the input into different parts by " = "
        String[] setClauseParts = set.split(" = ");

        // Extract the column name and value
        columnNameToSet = setClauseParts[0];
        valueToSet = setClauseParts[1].replaceAll("'", "");


        // Split the input into different parts by " = "
        String[] whereClauseParts = where.split(" = ");

        // Extract the column name and value
        columnNameToMatch = whereClauseParts[0];
        valueToMatch = whereClauseParts[1].replaceAll("'", "");

        return true;
    }

    public boolean updateTable(){
        TableStructure tableStructure = new TableStructure(schemaName, tableName);
        Map<String, Integer> columnNameToColumnIndexMap = tableStructure.getColumnNameToColumnIndexMap();

        setColumnIndex = columnNameToColumnIndexMap.get(columnNameToSet);
        matchColumnIndex = columnNameToColumnIndexMap.get(columnNameToMatch);

        Table table = new Table(schemaName,tableName);

        ArrayList<ArrayList<Object>> data = table.fetchTableData();

        for (ArrayList<Object> row : data) {
            if (row.get(matchColumnIndex).equals(valueToMatch)) {
                row.set(setColumnIndex, valueToSet);
            }
        }

        String stringTableData = Utils.buildInsertDataStringFromObjectArrayList(data);
        boolean isSuccess = table.insertTableData(stringTableData);

        if(isSuccess){
            ConsolePrinter.tableUpdateSuccess();
        }
        return isSuccess;
    }

}
