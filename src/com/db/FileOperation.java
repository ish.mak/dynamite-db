package com.db;

import java.io.File;
import java.io.IOException;

public class FileOperation {
    private String parentFolder;

    public FileOperation(){
        parentFolder = "storage";
    }

    private String getFilePath(String name){
        return parentFolder + "/" + name;
    }

    public boolean createFolder(String folderName){
        File newFolder = new File(getFilePath(folderName));
        return newFolder.mkdirs();
    }

    public boolean createFile(String fileName){
        File newFile = new File(getFilePath(fileName));

        try {
            return newFile.createNewFile();
        } catch(IOException e) {
            System.out.println("File creation failed:: " + fileName);
            return false;
        }
    }

    public boolean deleteFolder(String folderName){
        File folder = new File(getFilePath(folderName));
        deleteFilesInsideFolder(folderName);

        return folder.delete();

    }

    public boolean deleteFilesInsideFolder(String folderName){
        File folder = new File(getFilePath(folderName));

        File[] files = folder.listFiles();
        boolean isDeleted = false;

        if (files != null) {
            for (File file : files) {
                if (file.isFile()) {
                    isDeleted = file.delete();
                }
            }
        }

        return isDeleted;
    }
}
