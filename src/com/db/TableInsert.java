/**
The TableInsert class provides functionality to insert data into a database table.
*/
package com.db;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TableInsert {
    private String schemaName;
    private String tableName;
    private String insertTableString;
    private HashMap<String, Object> columnNameToValueMap;

    /**
     * Constructs an instance of TableInsert with the specified schema name and table name.
     *
     * @param schemaName the name of the database schema
     * @param tableName the name of the table
     */
    public TableInsert(String schemaName, String tableName){
        this.schemaName = schemaName;
        this.tableName = tableName;
    }

    /**
     * Constructs an instance of TableInsert with the specified insert statement.
     *
     * @param insertTableString the SQL insert statement
     */
    public TableInsert(String insertTableString){
        this.insertTableString = insertTableString;
    }

    /**
     * Parses the insert statement and sets the schema name, table name, and column name to value map.
     *
     * @return true if parsing was successful; false otherwise
     */
    public boolean parseQuery(){
        String newInsertTableString = insertTableString.replaceAll("\\n","");

        // Find the index of the opening parenthesis
        int index = newInsertTableString.indexOf('(');

        // Extract the first and second parts of the input string
        String firstPart = newInsertTableString.substring(0, index).trim();
        String[] splitWords = firstPart.split("\\s+"); // split on whitespace
        if (splitWords.length == 3 && splitWords[0].equalsIgnoreCase("INSERT") && splitWords[1].equalsIgnoreCase("INTO")) {
            String[] parts = splitWords[2].split("\\."); // split on dot
            schemaName = parts[0];
            tableName = parts[1];
        }else{
            ConsoleErrorPrinter.tableInsertInvalidStatement();
            return false;
        }

        int colStart = newInsertTableString.indexOf("(");
        int colEnd = newInsertTableString.indexOf(")", colStart);
        String columnsString = newInsertTableString.substring(colStart + 1, colEnd);

        // Find the substring between the second to last and last parentheses
        int valStart = newInsertTableString.lastIndexOf("(");
        int valEnd = newInsertTableString.lastIndexOf(")");
        String valuesString = newInsertTableString.substring(valStart + 1, valEnd);

        String[] columnsTokens = columnsString.split(",");
        ArrayList<String> columnsList = new ArrayList<>();
        for (String token : columnsTokens) {
            String newToken = token.replaceAll("'", "").trim();
            columnsList.add(newToken);
        }


        String[] valuesTokens = valuesString.split(",");
        ArrayList<String> valuesList = new ArrayList<>();
        for (String token : valuesTokens) {
            String newToken = token.replaceAll("'", "").trim();
            valuesList.add(newToken);
        }

        HashMap<String, Object> map = new HashMap<>();
        for (int i = 0; i < columnsList.size(); i++) {
            map.put(columnsList.get(i), valuesList.get(i));
        }

        columnNameToValueMap = map;
        return true;
    }

    /**
     * Inserts the row specified in the SQL insert statement into the database.
     *
     * @return true if insertion was successful; false otherwise
     */
    public boolean insertTable(){
        return insertSingleRow(columnNameToValueMap);
    }

    /**
     * Inserts a single row into a database table using the provided column name to value mapping.
     * @param columnNameToValueMap a HashMap representing the column name to value mapping for the row to be inserted
     * @return true if the row was successfully inserted, false otherwise
    */
    public boolean insertSingleRow(HashMap<String, Object> columnNameToValueMap){
        TableStructure tableStructure = new TableStructure(schemaName,tableName);
        Map<String, String> columnNameToDatatypeMap = tableStructure.getColumnNameToDatatypeMap();
        Map<String, Boolean> columnNameToNotNullMap =  tableStructure.getColumnNameToNotNullMap();
        Map<String, Boolean> columnNameToAutoIncrementMap = tableStructure.getColumnNameToAutoIncrementMap();

        Boolean isValid;

        // check if values and datatype is matching or not
        TableColumnDataType tableColumnDataType = new TableColumnDataType(columnNameToValueMap, columnNameToDatatypeMap);
        isValid = tableColumnDataType.isValidInput();
        if(!isValid) return false;

        // verify that not null columns should not be set to null
        TableColumnNotNull tableColumnNotNull = new TableColumnNotNull(columnNameToValueMap, columnNameToNotNullMap);
        isValid = tableColumnNotNull.isValidInput();
        if(!isValid){
            ConsoleErrorPrinter.tableInsertNotNullError();
            return false;
        }


        // ---------- STEP 1 ----------
        // get table data
        // get last row of data for auto-increment purpose

        // ---------- STEP 2 ----------
        // iterate through all the columns
        //  if column is not auto-increment then set it with its respective value
        //  if column is auto-increment
            // if there exists previous row then increment that
            // if current data is first entry in table then start auto-increment column with 1

        // ---------- STEP 3 ----------
        // add new row to table
        // set data in table


        // ---------- CODE: STEP 1 ----------
        // get table data
        Table table = new Table(schemaName, tableName);
        ArrayList<ArrayList<Object>> tableData = table.fetchTableData();

        // get lastRow
        ArrayList<Object> lastRow = new ArrayList<Object>();
        if(tableData.size() > 0){
            lastRow = tableData.get(tableData.size() - 1);
        }

        boolean hasLastRowInTable = !lastRow.isEmpty();

        // new row that needs to be added in the database
        ArrayList<Object> newRow = new ArrayList<Object>();

        // ---------- CODE: STEP 2 ----------
        // iterate through all the columnNames and create new row as ArrayList
        int columnIndex = 0;
        for (String columnName : columnNameToDatatypeMap.keySet()) {
            // auto increment part
            Boolean isColumnSetToAutoIncrement = columnNameToAutoIncrementMap.get(columnName);
            if(isColumnSetToAutoIncrement){
                if(hasLastRowInTable){
                    int lastRowIndex = Integer.parseInt((String) lastRow.get(columnIndex));
                    newRow.add(lastRowIndex + 1);
                }else{
                    newRow.add(1);
                }

                columnIndex ++;
                continue;
            }

            newRow.add(columnNameToValueMap.get(columnName));

            columnIndex ++;
        }

        // ---------- CODE: STEP 3 ----------
        tableData.add(newRow);
        String stringTableData = Utils.buildInsertDataStringFromObjectArrayList(tableData);
        Boolean isSuccess =  table.insertTableData(stringTableData);
        if(isSuccess){
            ConsolePrinter.tableInsertSuccess();
        }
        return isSuccess;
    }
}
