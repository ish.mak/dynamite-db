package com.db;

public class DatabaseCreate {
    String createDBString;
    String databaseName;

    public DatabaseCreate(String createDBString){
        this.createDBString = createDBString;
    }

    public boolean parseQuery(){
        String[] splitWords = createDBString.split("\\s+"); // split on whitespace

        if (splitWords.length == 3 && splitWords[0].equalsIgnoreCase("CREATE") && splitWords[1].equalsIgnoreCase("DATABASE")) {
            databaseName = splitWords[2].replaceAll(";", "");
            return true;
        } else {
            ConsoleErrorPrinter.databaseCreateInvalidStatement();
            return false;
        }
    }

    public boolean createDatabase(){
        FileOperation fileOperation = new FileOperation();
        Boolean isSuccess = fileOperation.createFolder(databaseName);

        if(isSuccess){
            ConsolePrinter.databaseCreateSuccess();
            return true;
        }else{
            ConsoleErrorPrinter.databaseCreateFailure();
            return false;
        }
    }
}
