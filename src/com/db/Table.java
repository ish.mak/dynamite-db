package com.db;

import java.util.ArrayList;

public class Table {
    private String tableName;
    private String schemaName;

    public Table(String schemaName, String tableName){
        this.schemaName = schemaName;
        this.tableName = tableName;
    }

    private String appendParentDir(String filePath){
        return "storage/" + filePath;
    }

    private String getTableDataFilePath(){
        return appendParentDir(schemaName + "/" + tableName + "/" + "data.dsdsv");
    }

    private String getTableStructureFilePath(){
        return appendParentDir(schemaName + "/" + tableName + "/" + "structure.dsdsv");
    }

    public ArrayList<ArrayList<Object>> fetchTableData(){
        String filePath = getTableDataFilePath();
        return FileData.fetchDataFromFile(filePath);
    }

    public ArrayList<ArrayList<Object>> fetchTableStructure(){
        String filePath = getTableStructureFilePath();
        return FileData.fetchDataFromFile(filePath);
    }

    public boolean insertTableData(String stringData){
        String fp = schemaName + "/" + tableName + "/data.dsdsv";
        String filePath = appendParentDir(fp);

        return FileData.insertDataInFile(filePath, stringData, tableName);
    }

    public boolean insertTableStructure(String stringData){
        String fp = schemaName + "/" + tableName + "/structure.dsdsv";
        String filePath = appendParentDir(fp);

        return FileData.insertDataInFile(filePath, stringData, tableName);
    }
}
