/**
 * The TableCreate class is responsible for creating tables in the database system.
 */
package com.db;

import java.util.ArrayList;
import java.util.Arrays;

public class TableCreate {
    String createTableString;
    String schemaName;
    String tableName;
    ArrayList<ArrayList<String>> tableArrayList;

    /**
     * Constructor that initializes the createTableString variable.
     *
     * @param createTableString a string containing the create table statement
     */
    public TableCreate(String createTableString){
        this.createTableString = createTableString;
    }

    /**
     * Parses the createTableString to extract the schema name, table name and columns and stores the
     * result in an ArrayList.
     *
     * @return true if parsing is successful, false otherwise
     */
    public boolean parseQuery(){
        // remove all new lines
        String newCreateTableString = createTableString.replaceAll("\\n","");

        // Find the index of the opening parenthesis
        int index = newCreateTableString.indexOf('(');

        // Extract the first and second parts of the input string
        String firstPart = newCreateTableString.substring(0, index).trim();
        String secondPart = newCreateTableString.substring(index).trim();

        String[] splitWords = firstPart.split("\\s+"); // split on whitespace

        if (splitWords.length == 3 && splitWords[0].equalsIgnoreCase("CREATE") && splitWords[1].equalsIgnoreCase("TABLE")) {
            String[] parts = splitWords[2].split("\\."); // split on dot
            schemaName = parts[0];
            tableName = parts[1];
        }else{
            ConsoleErrorPrinter.tableCreateInvalidStatement();
            return false;
        }

        String removedBracesAndSemicolonString = secondPart.replace("(", "").replace(")", "").replace(";", "").trim();
        String[] columns = removedBracesAndSemicolonString.trim().split("\\s*,\\s*");


        // define array list of 4 rows and columns length
        // 0,0 index write COLUMN_NAME
        // 1,0 index write DATA_TYPE
        // 2,0 index write AUTO_INCREMENT
        // 3,0 index write NOT_NULL

        ArrayList<ArrayList<String>> table = new ArrayList<>();

        // Create the table with the specified number of rows and columns
        for (int i = 0; i < 4; i++) {
            ArrayList<String> row = new ArrayList<>();
            for (int j = 0; j < 1; j++) {
                row.add("");
            }
            table.add(row);
        }
        // Write values to the specified indices
        table.get(0).set(0, "COLUMN_NAME");
        table.get(1).set(0, "DATA_TYPE");
        table.get(2).set(0, "AUTO_INCREMENT");
        table.get(3).set(0, "NOT_NULL");

        for (int i = 0; i < columns.length; i++) {
            String[] parts = columns[i].split(" ");


            // set column name
            table.get(0).add(parts[0]);

            // set data type
            table.get(1).add(parts[1]);

            // if auto increment then set it, otherwise empty string
            if(columns[i].contains("AUTO_INCREMENT")){
                table.get(2).add("YES");
            }else{
                table.get(2).add("NO");
            }

            // if not null then set it, otherwise empty string
            if(columns[i].contains("NOT NULL")){
                table.get(3).add("YES");
            }else{
                table.get(3).add("NO");
            }
        }

        tableArrayList = table;

        return true;
    }

    /**
     Creates a new table in the specified schema with the given name.
     This method creates a new directory for the table inside the specified schema directory.
     It also creates two files, "data.dsdsv" and "structure.dsdsv", inside the newly created table directory.
     The "data.dsdsv" file will store the actual data rows of the table, while the "structure.dsdsv" file
     will store the schema or structure of the table. The schema of the table is defined by the array list
     of string representation of table columns passed as parameter to this method.
     @return true if the table was successfully created, false otherwise
     @see FileOperation#createFolder(String)
     @see FileOperation#createFile(String)
     @see Table#insertTableStructure(String)
     @see Utils#buildInsertDataStringFromStringArrayList(ArrayList)
     @see ConsoleErrorPrinter#tableCreateFailed()
     */
    public boolean createTable(){
        // create table folder inside schema
        // if table already exist then display error and return false
        FileOperation fileOperation = new FileOperation();

        String tableFolderPath = schemaName + "/" + tableName;
        Boolean isSuccess = fileOperation.createFolder(tableFolderPath);

        if(!isSuccess){
            ConsoleErrorPrinter.tableCreateFailed();
        }

        // create data.dsdsv file and structure.dsdsv file
        isSuccess = fileOperation.createFile(tableFolderPath + "/data.dsdsv");
        if(!isSuccess){
            ConsoleErrorPrinter.tableCreateFailed();
        }
        isSuccess = fileOperation.createFile(tableFolderPath + "/structure.dsdsv");
        if(!isSuccess){
            ConsoleErrorPrinter.tableCreateFailed();
        }


        // write tableArrayList to structure.dsdsv
        String stringStructureData = Utils.buildInsertDataStringFromStringArrayList(tableArrayList);
        Table table = new Table(schemaName, tableName);
        isSuccess = table.insertTableStructure(stringStructureData);

        if(!isSuccess){
            ConsoleErrorPrinter.tableCreateFailed();
        }else{
            ConsolePrinter.tableCreateSuccess();
        }

        return isSuccess;
    }
}
