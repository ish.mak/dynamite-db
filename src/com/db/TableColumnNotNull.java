/**
 The {@code TableColumnNotNull} class represents the validation of a not null constraint for a table column.
 It contains methods to check whether a given column's value complies with its not null constraint or not.
 */
package com.db;

import java.util.Map;

public class TableColumnNotNull {
    Map<String, Object> columnNameToValueMap;
    Map<String, Boolean> columnNameToNotNullMap;

    /**
     * Constructs a {@code TableColumnNotNull} object with the given column name to value and column name to not null mappings.
     *
     * @param columnNameToValueMap a mapping of column name to its value.
     * @param columnNameToNotNullMap a mapping of column name to a boolean indicating whether the column is set to not null or not.
     */
    public TableColumnNotNull(Map<String, Object> columnNameToValueMap, Map<String, Boolean> columnNameToNotNullMap){
        this.columnNameToValueMap = columnNameToValueMap;
        this.columnNameToNotNullMap = columnNameToNotNullMap;
    }

    /**
     * Checks whether the given input value is valid or not for the corresponding not null constraint of a column.
     * If the value of a column marked as not null is empty or null, an error message is printed and false is returned.
     *
     * @return true if the input values for all columns marked as not null are not empty or null, false otherwise.
     */
    public boolean isValidInput(){
        for (String columnName : columnNameToValueMap.keySet()) {
            Boolean isColumnSetToNotNull = columnNameToNotNullMap.get(columnName);

            if(isColumnSetToNotNull){
                // value should not be empty
                Boolean isObjectEmpty = Utils.isObjectEmpty(columnNameToValueMap.get(columnName));
                if(isObjectEmpty){
                    ConsoleErrorPrinter.tableColumnNotNullError(columnName);
                    return false;
                }
            }
        }

        return true;
    }
}
