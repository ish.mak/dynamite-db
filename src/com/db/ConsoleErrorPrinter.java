package com.db;

public class ConsoleErrorPrinter {
    public static void authenticationLoginFailure(){
        System.out.print("\n");
        System.out.println("Invalid Credentials...");
        System.out.println("Please try to login again or Sign Up if you don't have account...");
        System.out.print("\n");
    }

    public static void tableColumnDataTypeError(Object value, String dataType){
        System.out.println("***** ERROR *****");
        System.out.println("Datatype and Value didn't matched");
        System.out.println("Datatype: " + dataType);
        System.out.println("Input Value: " + value);
        System.out.println("***** PLEASE TRY AGAIN *****");
    }

    public static void tableColumnNotNullError(String columnName){
        System.out.println("***** ERROR *****");
        System.out.println("The input value found null for below column");
        System.out.println("Column Name: " + columnName);
        System.out.println("***** PLEASE TRY AGAIN *****");
    }

    public static void databaseCreateInvalidStatement(){
        System.out.println("Invalid CREATE DATABASE statement...");
    }

    public static void databaseCreateFailure(){
        System.out.println("Something went wrong. Failed to create database...");
    }

    public static void tableCreateInvalidStatement(){
        System.out.println("Invalid CREATE TABLE statement...");
    }

    public static void tableInsertInvalidStatement(){
        System.out.println("Invalid INSERT statement...");
    }

    public static void tableCreateFailed(){
        System.out.println("Table creation failed...");
    }

    public static void tableInsertNotNullError(){
        System.out.print("\n");
        System.out.println("One of the column has a constraint of NOT NULL, but the value passed is null");
        System.out.print("\n");
    }
}
