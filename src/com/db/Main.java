package com.db;

public class Main {

    /**
     * The main method creates instances of the Authentication and DatabaseOperation classes, 
     * and calls their respective methods loginOrSignUp and selectDatabaseOperation.
     *
     * @param args an array of command-line arguments passed to the application
     */
    public static void main(String[] args) {
        Authentication auth = new Authentication();
        auth.loginOrSignUp();

        DatabaseOperation dbOperation = new DatabaseOperation();
        dbOperation.selectDatabaseOperation();
    }
}