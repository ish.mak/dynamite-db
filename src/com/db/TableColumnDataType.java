/**
 Represents the data types of each column in a table along with their corresponding values.
 */
package com.db;

import java.util.Map;

public class TableColumnDataType {
    Map<String, Object> columnNameToValueMap;
    Map<String, String> columnNameToDatatypeMap;

    /**
     * Constructor for creating a TableColumnDataType object.
     * @param columnNameToValueMap A map of column names to their corresponding values.
     * @param columnNameToDatatypeMap A map of column names to their corresponding data types.
     */
    public TableColumnDataType(Map<String, Object> columnNameToValueMap, Map<String, String> columnNameToDatatypeMap){
        this.columnNameToValueMap = columnNameToValueMap;
        this.columnNameToDatatypeMap = columnNameToDatatypeMap;
    }

    /**
     * Checks whether the input values are valid for their respective data types.
     * @return True if the input values are valid, false otherwise.
     */
    public boolean isValidInput(){
        for (String columnName : columnNameToValueMap.keySet()) {
            Object value = columnNameToValueMap.get(columnName);
            String dataType = columnNameToDatatypeMap.get(columnName);

            boolean isValueAndColumnDatatypeMatching = verifyValueAndDataType(value, dataType);
            if(!isValueAndColumnDatatypeMatching){
                ConsoleErrorPrinter.tableColumnDataTypeError(value, dataType);
                return false;
            }
        }

        return true;
    }

    /**
     * Verifies if the given value is of the correct data type.
     * @param value The value to be verified.
     * @param dataType The expected data type of the value.
     * @return True if the value is of the correct data type, false otherwise.
     */
    private boolean verifyValueAndDataType(Object value, String dataType){
        switch(dataType) {
            case "INT":
                return value instanceof Integer;
            case "VARCHAR":
                return value instanceof String;
            default:
                return false;
        }
    }
}
