/**
 A class representing a SELECT query on a table
 */
package com.db;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TableSelect {
    private String selectTableString;
    private String schemaName;
    private String tableName;

    /**
     * Constructor for TableSelect class
     * @param selectTableString a string representing the SELECT query to be executed on the table
     */
    public TableSelect(String selectTableString) {
        this.selectTableString = selectTableString;
    }

    /**
     * Method to parse the SELECT query and extract the table schema and table name
     * @return true if the query was parsed successfully, false otherwise
     */
    public boolean parseQuery() {
        String newSelectTableString = selectTableString.replaceAll("\\n", "");
        newSelectTableString = newSelectTableString.replaceAll(";","");

        String[] splitQuery = newSelectTableString.split("\\s+");
        String[] parts = splitQuery[3].split("\\.");
        schemaName = parts[0];
        tableName = parts[1];

        if (splitQuery[0].equalsIgnoreCase("SELECT")) {
            if (schemaName != null && tableName != null) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method to select data from the table based on the SELECT query provided
     * @return true if the data was successfully retrieved and printed, false otherwise
     */
    public boolean selectTable(){
        Table table = new Table(schemaName, tableName);
        ArrayList<ArrayList<Object>> data = table.fetchTableData();

        TableStructure tableStructure = new TableStructure(schemaName,tableName);
        Map<String, String> columnNameToDatatypeMap = tableStructure.getColumnNameToDatatypeMap();

        List<String> columnNames = new ArrayList<>(columnNameToDatatypeMap.keySet());
        List<String> dataTypes = new ArrayList<>(columnNameToDatatypeMap.values());

        // Print column headers
        for (String columnName : columnNames.subList(0, columnNames.size())) {
            System.out.printf("%-15s", columnName.toUpperCase());
        }
        System.out.println();

        // Print data types
        for (String dataType : dataTypes.subList(0, dataTypes.size())) {
            System.out.printf("%-15s", dataType.toUpperCase());
        }
        System.out.println();

        // Print rows
        for (ArrayList<Object> row : data) {
            for (int i = 0; i < row.size(); i++) {

                System.out.printf("%-15s", row.get(i).toString());
            }
            System.out.println();
        }

        ConsolePrinter.printNewLine();
        return true;
    }
}
