package com.db;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

public class TableStructure {
    final String COLUMN_NAME = "COLUMN_NAME";
    final String DATA_TYPE = "DATA_TYPE";
    final String NOT_NULL = "NOT_NULL";
    final String AUTO_INCREMENT = "AUTO_INCREMENT";

    private Map<String, String> columnNameToDatatype;
    private Map<String, Boolean> columnNameToNotNull;
    private Map<String, Boolean> columnNameToAutoIncrement;
    private Map<String, Integer> columnNameToColumnIndex;

    ArrayList<ArrayList<Object>> tableStructure;

    public TableStructure(String schemaName, String tableName) {
        Table table = new Table(schemaName, tableName);
        tableStructure = table.fetchTableStructure();


        columnNameToDatatype = columnNameToDatatypeMap();
        columnNameToNotNull = columnNameToNotNullMap();
        columnNameToAutoIncrement = columnNameToAutoIncrementMap();
        columnNameToColumnIndex = columnNameToColumnIndexMap();

    }


    private ArrayList<Object> getRowByType(String type){
        ArrayList<Object> resultRow = new ArrayList<>();


        // loop through the data
        for (int i = 0; i < tableStructure.size(); i++) {

            ArrayList<Object> row = tableStructure.get(i);

            String structureFirstColumn = row.get(0).toString();
            if(structureFirstColumn.equals(type)){
                resultRow = row;
            }
        }

        return resultRow;
    }


    private Map<String, String> columnNameToDatatypeMap(){
        ArrayList<Object> columnNames = getRowByType(COLUMN_NAME);
        ArrayList<Object> dataTypes = getRowByType(DATA_TYPE);

        Map<String, String> columnNameToDatatypeMap = new LinkedHashMap<>();
        for (int i = 1; i < columnNames.size(); i++) {
            String key = columnNames.get(i).toString();
            String value = dataTypes.get(i).toString();
            columnNameToDatatypeMap.put(key, value);
        }

        return columnNameToDatatypeMap;
    }

    private Map<String, Boolean> columnNameToNotNullMap(){
        ArrayList<Object> columnNames = getRowByType(COLUMN_NAME);
        ArrayList<Object> notNulls = getRowByType(NOT_NULL);

        Map<String, Boolean> columnNameToNotNullMap = new LinkedHashMap<>();
        for (int i = 1; i < columnNames.size(); i++) {
            String key = columnNames.get(i).toString();
            Boolean value = notNulls.get(i).toString().equals("YES");
            columnNameToNotNullMap.put(key, value);
        }

        return columnNameToNotNullMap;
    }

    private Map<String, Boolean> columnNameToAutoIncrementMap(){
        ArrayList<Object> columnNames = getRowByType(COLUMN_NAME);
        ArrayList<Object> autoIncrements = getRowByType(AUTO_INCREMENT);

        Map<String, Boolean> columnNameToAutoIncrementMap = new LinkedHashMap<>();
        for (int i = 1; i < columnNames.size(); i++) {
            String key = columnNames.get(i).toString();
            Boolean value = autoIncrements.get(i).toString().equals("YES");
            columnNameToAutoIncrementMap.put(key, value);
        }

        return columnNameToAutoIncrementMap;
    }

    private Map<String, Integer> columnNameToColumnIndexMap(){
        ArrayList<Object> columnNames = getRowByType(COLUMN_NAME);

        Map<String, Integer> columnNameToColumnIndexMap = new LinkedHashMap<>();
        for (int i = 1; i < columnNames.size(); i++) {
            String key = columnNames.get(i).toString();
            columnNameToColumnIndexMap.put(key, i-1);
        }

        return columnNameToColumnIndexMap;
    }


    public Map<String, String> getColumnNameToDatatypeMap() {
        Logger.tableStructureGetColumnNameToDatatypeMap(columnNameToDatatype);
        return columnNameToDatatype;
    }

    public Map<String, Boolean> getColumnNameToNotNullMap() {
        Logger.tableStructureGetColumnNameToNotNullMap(columnNameToNotNull);
        return columnNameToNotNull;
    }

    public Map<String, Boolean> getColumnNameToAutoIncrementMap() {
        Logger.tableStructureGetColumnNameToAutoIncrementMap(columnNameToAutoIncrement);
        return columnNameToAutoIncrement;
    }

    public Map<String, Integer> getColumnNameToColumnIndexMap(){
        return columnNameToColumnIndex;
    }
}
