package com.db;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Authentication {

    Scanner scanner;
    public Authentication(){
        scanner = new Scanner(System.in);
    }

    public void loginOrSignUp(){
        int choice;

        ConsolePrinter.authenticationLoginOrSignUpHeader();
        ConsolePrinter.authenticationLoginOrSignUpChoiceSelection();

        choice = scanner.nextInt();
        scanner.nextLine();


        if(choice != 1 && choice != 2){
            System.out.println("Invalid choice");
            System.out.println("****************************");
            loginOrSignUp();
        }

        if(choice == 1){
            login();
        }else if(choice == 2){
            signUp();
        }
    }


    public String inputUserName() {
        ConsolePrinter.authenticationSignUpLoginUsername();
        String username = scanner.nextLine();

        Table table = new Table("auth", "user");
        ArrayList<ArrayList<Object>> data = table.fetchTableData();
        String nameToFind = username;
        boolean userExists = false;

        for (ArrayList<Object> row : data) {
            if (row.get(1).equals(nameToFind)) {
                userExists = true;
                break;
            }
        }

        if(userExists){
            System.out.println("User with name " + nameToFind + " exists. Please re-enter another name");
            inputUserName();
        }

        return username;
    }


    public void signUp() {
        ConsolePrinter.authenticationSignUpHeader();
        String username = inputUserName();

        ConsolePrinter.authenticationSignUpLoginPassword();
        String password = scanner.nextLine();
        String hashedPassword = Utils.getMd5Hash(password);

        ConsolePrinter.authenticationSignUpLogin2FA();
        String birthCity = scanner.nextLine();


        HashMap<String, Object> columnNameToValue = new HashMap<String, Object>();

        // Add values
        columnNameToValue.put("username", username);
        columnNameToValue.put("password", hashedPassword);
        columnNameToValue.put("answer", birthCity);

        TableInsert tableInsert = new TableInsert("auth", "user");
        tableInsert.insertSingleRow(columnNameToValue);
        ConsolePrinter.authenticationSignUpSuccess();
    }

    public void login(){
        ConsolePrinter.authenticationLoginHeader();
        ConsolePrinter.authenticationSignUpLoginUsername();
        String username = scanner.nextLine();

        ConsolePrinter.authenticationSignUpLoginPassword();
        String password = scanner.nextLine();
        String hashedPassword = Utils.getMd5Hash(password);

        ConsolePrinter.authenticationSignUpLogin2FA();
        String birthCity = scanner.nextLine();

        Table table = new Table("auth", "user");
        ArrayList<ArrayList<Object>> data = table.fetchTableData();

        String nameToMatch = username;
        String passwordToMatch = hashedPassword;
        String birthCityToMatch = birthCity;

        boolean userExists = false;

        for (ArrayList<Object> row : data) {
            if (row.get(1).equals(nameToMatch) && row.get(2).equals(passwordToMatch) && row.get(3).equals(birthCityToMatch)) {
                userExists = true;
                break;
            }
        }

        if(userExists){
            ConsolePrinter.authenticationLoginSuccess();
        }else{
            ConsoleErrorPrinter.authenticationLoginFailure();
            loginOrSignUp();
        }
    }
}
