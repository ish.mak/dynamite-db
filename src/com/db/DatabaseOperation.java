package com.db;

import java.util.Scanner;

public class DatabaseOperation {

    Scanner scanner;
    public DatabaseOperation(){
        scanner = new Scanner(System.in);
    }

    public void selectDatabaseOperation(){
        ConsolePrinter.databaseOperationSelectHeader();
        ConsolePrinter.databaseOperationSelectOptions();
        int choice = scanner.nextInt();
        scanner.nextLine();

        ConsolePrinter.printNewLine();

        switch(choice){
            case 1: {
                ConsolePrinter.databaseOperationSelectedOption("CREATE DATABASE");

                // 1 - Get Input Query
                String createDBString = Utils.getMultiLineInputFromConsole();

                // 2 - Parse Query
                DatabaseCreate databaseCreate = new DatabaseCreate(createDBString);

                Boolean isSuccess = false;
                isSuccess = databaseCreate.parseQuery();

                if(!isSuccess){
                    selectDatabaseOperation();
                    break;
                }

                // 3 - Create Database
                databaseCreate.createDatabase();

                selectDatabaseOperation();
                break;
            }
            case 2: {
                ConsolePrinter.databaseOperationSelectedOption("CREATE TABLE");

                // 1 - Get Input Query
                String createTableString = Utils.getMultiLineInputFromConsole();

                // 2 - Parse Query
                TableCreate tableCreate = new TableCreate(createTableString);

                Boolean isSuccess = false;
                isSuccess = tableCreate.parseQuery();
                if(!isSuccess) {
                    selectDatabaseOperation();
                    break;
                }

                // 3 - Create Table
                tableCreate.createTable();

                selectDatabaseOperation();
                break;
            }
            case 3: {
                ConsolePrinter.databaseOperationSelectedOption("SELECT");
                // 1 - Get Input Query
                String selectTableString = Utils.getMultiLineInputFromConsole();

                // 2 - Parse Query
                TableSelect tableSelect = new TableSelect(selectTableString);

                Boolean isSuccess = false;
                isSuccess = tableSelect.parseQuery();
                if(!isSuccess) {
                    selectDatabaseOperation();
                    break;
                }

                // 3 - Select and Display
                tableSelect.selectTable();

                selectDatabaseOperation();
                break;
            }
            case 4: {
                ConsolePrinter.databaseOperationSelectedOption("INSERT");
                // 1 - Get Input Query
                String insertTableString = Utils.getMultiLineInputFromConsole();


                // 2 - Parse Query
                TableInsert tableInsert = new TableInsert(insertTableString);

                Boolean isSuccess = false;
                isSuccess = tableInsert.parseQuery();
                if(!isSuccess) {
                    selectDatabaseOperation();
                    break;
                }

                // 3 - Insert Table
                tableInsert.insertTable();
                
                selectDatabaseOperation();
                break;
            }
            case 5: {
                ConsolePrinter.databaseOperationSelectedOption("UPDATE");

                // 1 - Get Update Query
                String updateTableString = Utils.getMultiLineInputFromConsole();

                // 2 - Parse Query
                TableUpdate tableUpdate = new TableUpdate(updateTableString);
                Boolean isSuccess = false;
                isSuccess = tableUpdate.parseQuery();
                if(!isSuccess) {
                    selectDatabaseOperation();
                    break;
                }

                // 3 - Update Table
                tableUpdate.updateTable();

                selectDatabaseOperation();
                break;

            }
            case 6: {
                ConsolePrinter.databaseOperationSelectedOption("DELETE");
            }
            case 7: {
                break;
            }
            default:{

            }
        }


    }

}
