## Objective
The goal of this project is to develop a lightweight DBMS using the Java programming language.

## Functionalities
The application should have the following functionalities:

1.  User Authentication Module that implements two-factor authentication using ID, Password, and question/answer for authentication.
2. Design of persistent storage that uses a custom-designed file format to store data, user information. (The persistent storage must use a custom file format that is not based on commonly used file formats such as JSON, XML, or CSV. This file format should have custom delimiters to store and access data)
3. Implementation of Queries Data Definition Language (DDL) & Data Manipulation Language (DML).
4. Implementation of NULL Constraint and AUTO_INCREMENT Constraint.

The code should be modular - allowing for easy maintenance and future development.